package me.icocoa;

/**
 * Main class.
 *
 * @author Lingchao Xin
 */
public class Jist {
    public static void main(String[] args) {
	JistParser.getInstance().run(args);
    }
}
