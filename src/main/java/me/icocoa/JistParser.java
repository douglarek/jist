package me.icocoa;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.ParserProperties;

import java.util.List;
import java.util.ArrayList;

/**
 * A singleton parser class for Jist.
 *
 * @author Lingchao Xin
 */
public class JistParser {

    @Option(name="-l", usage="List public gists, with `-A` list all ones")
    private boolean list;

    @Option(name="-A", depends={"-l"})
    private boolean all;

    @Option(name="-h", aliases={"--help"}, help=true, usage="Show this message and exit.")
    private boolean help;

    @Argument
    private List<String> arguments = new ArrayList<String>();

    private final CmdLineParser parser;

    private static class JistParserHolder {
	private static final JistParser INSTANCE = new JistParser();
    }

    private JistParser () {
	parser = new CmdLineParser(this, ParserProperties.defaults().withOptionSorter(null));
    }

    public static final JistParser getInstance() {
	return JistParserHolder.INSTANCE;
    }

    private void usage() {
	System.out.println("Usage: jist [OPTIONS] [FILES]...\n\nOptions:");
	parser.printUsage(System.out);
    }

    public void run(String[] args) {
	try {
	    parser.parseArgument(args);

	    if (help) {
		usage();
	    } else if (! arguments.isEmpty()) {
		System.out.println(arguments);
	    } else if (list) {

	    } else if (all) {

	    } else {
		usage();
	    }
	}
	catch (CmdLineException cle) {
	    System.err.println(cle.getMessage());
	}
    }
}
